# Introduction aux science frugale à travers l'étude du foldscope
## Présentation

Je m'appelle Simon, j'ai 24 ans et je suis en master de physique à l'Université Libre de Bruxelles. Curieux et créatif, j'ai toujours fait beaucoup d'activités en parallèle de mes cours.
Fils de parents expatriés, j'ai grandi au Mozambique. Cette enfance en Afrique m'a vite introduite aux sciences frugales sans pour autant le savoir. En effet, les populations locales manquent de moyens et bricolent des alternatives créatives, simples et ingénieuses aux objets du quotidien.

## Introduction

Dans le cadre du cours technique avancées de physique expérimentale l'étude du [foldscope](https://fr.wikipedia.org/wiki/foldscope) nous sert d'introduction aux sciences frugales. Inventé en 2012 par Manu Prakash, le foldscope est un microscope optique à moins d'un euro permettant un grossissement de 2 000 fois.

Le cadre plus général dans lequel s'insère cette démarche est un projet qui vise à surveiller la pollution dans la baie de la Havane à Cuba. L'idée, c'est de s'inspirer du foldscope pour créer une bouée frugale qui mesure la concentration de différents composés présents dans l'eau.
Nous sommes un groupe de [cinq étudiants](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students) et à raison d'une réunion par semaine, nous allons étudier en équipe le foldscope. L'objectif est de **maitriser** l'outil afin de le **dépasser**.

En parallèle, nous allons chacun creuser un aspect différent du foldscope. Je détaillerais une adaptation minimaliste et user friendly de celui-ci. Cette partie est résumée sous forme d'un tutoriel "A user friendly guide for frugal microscopy in the field", rédigé en anglais que vous trouverez dans le dossier du même nom.
Nous tiendrons ici un journal de bord qui reflétera l'avancement de notre réflexion et de nos discussions au fil des réunions.

Avant de commencer, je tiens à préciser que c'est avec beaucoup d'enthousiasme que je me lance dans ce projet d'équipe. Il apporte du contraste et de la couleur, tout deux bienvenus à mon cursus très linéaire de physicien théoricien.

## Journal de bord

## Lundi 26 septembre 2022

En préparation de la première séance, nous devions lire [l'article](https://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0098781) de Manu Prakash  et identifier des aspects techniques remarquables du foldscope. \
Voici les aspects qui m'ont particulièrement marqué que je regroupe dans trois catégories :

- **Mobilité**: Le foldscope offre une mobilité remarquable. On peut facilement déplacer l'échantillon sur le plan (x,y) grâce à des simples mouvements de pouces. La mise au point de la lentille se fait grâce à un mouvement tout aussi intuitif le long de l'axe z. Pas besoin de systèmes complexes et fragiles, le pliage origami est un moyen ingénieux pour arriver à cette souplesse de mouvements. Malgré son fonctionnement simple, le foldscope est un outil précis grâce aux contraintes imposées par son pliage.

- **Distribution et Robustesse** : De par sa construction, une simple feuille 2D qui s'assemble à l'aide d'un pliage, le foldscope est un objet qui peut se produire en masse pour un faible coût. Leger et compact, on peut le distribuer rapidement aux quatre coins de la planète. Son fonctionnement très simple le rend aussi très résistant et contrairement à un microscope classique, on peut l'utiliser sur le terrain sans risquer de l'abîmer. Le foldscope est fournis avec une petite lampe LED et une batterie, ce qui le libère de la contrainte d'une source d'électricité.

- **Frugalité** : Le foldscope est un outil très facile à prendre en main. Il garde le cœur d'un microscope classique tout en laissant le gadget de côté, ce qui rend son utilisation facile et intuitive. Les différentes parties du foldscope s'alignent et s'agencent automatiquement grâce à des aimants puissants qui font quasi tout le travail à la place de l'utilisateur.

La première séance a commencé par un brainstorming sur les aspects remarquables du foldscope. Nous avons parlé de la technique de l'outil avec la fabrication de sa lentille, l'ingéniosité de son pliage, les différents types de microscopie disponible et la technologie LED utilisée. Nous avons également discuté des applications du foldscope en passant par la détection des différents type de malaria pour arriver sur ses qualités en tant qu'outil éducatif.

Après le brainstorming, nous avons essayé d'en assembler un. À l'image d'un meuble IKEA, le foldscope se construit à l'aide d'un manuel de pictogrammes instructifs muni d'un code couleur. J'étais accompagné de [Danaé](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/danae.valdenaire) lors de cette étape. Nous avons d'abord lu les instructions et puis nous avons essayé de l'assembler. 

Cette étape s'est avérée plus difficile que prévu. Il nous manquait plusieurs pièces, que nous avons finalement trouvé chez nos camarades. Pour continuer avec l'analogie IKEA, les instructions ne sont pas très précises et laissent souvent place à l'interprétation subjective. Danaé et moi n'étions pas toujours d'accord sur la démarche à suivre. Nous avons pris plus de temps que prévu et nous avons finalement dû finir son assemblage après la réunion.

Voici l'objet final que nous avons construit après une vingtaine de minutes :

![fold](image/fold.jpg)

Cette séance m'a introduit aux sciences frugales et j'ai compris leur absolue nécessité. Dans un monde encore inégalitaire et dont les enjeux écologiques poussent à la fin d'une abondance, elles apparaissent comme des outils indispensables pour aborder les défis de demain.

## Lundi 3 octobre 2022

Nous avons commencé la deuxième séance par passer en revue les différents journaux de bord. Un des piliers des sciences frugales, c'est le travail en communauté articulé par des projets collaboratifs. La communication joue donc un rôle essentiel dans leur développement. Après plusieurs remarques constructives, nous avons reçu des conseils concernant notre rédaction.

Après ce tour de table, nous avons chacun entamé une recherche dans la littérature scientifique sur les applications du foldscope. L'objectif est de faire un état de l'art des possibilités qu'offre l'outil. Pour ne pas passer toute la réunion sur cette étude, nous avons prévu de terminer cet exercice plus tard et d'en discuter lors de notre prochain rendez-vous.

Nous avons profité du temps qu'il nous restait pour manipuler le foldscope et faire une mesure expérimentale. J'ai choisi de mesurer l'épaisseur d'une patte de mouche. Pour cela, j'ai eu besoin d'une lame avec un quadrillage de 0.5 mm de côté. J'ai calculé le nombre de pixels qui correspondent à une graduation, ce qui m'a donné une correspondance avec leur taille réelle. Ensuite, en calculant le nombre de pixels sur la patte, j'ai pu calculer son épaisseur. J'ai fait attention à placer la patte au centre de la lentille afin de minimiser les effets d'aberrations.

Après 40 mesures, j'ai calculé une moyenne et un écart-type. J'ai rajouté une erreur supplémentaire qui prend en compte la qualité de l'image, le logiciel utilisé et l'angle avec lequel je fais la photo. Au final, j'estime l'épaisseur de la patte de mouche à 170 +- 15 microns.


Mesure de la grille        |  Mesure de la patte
:-------------------------:|:-------------------------:
![](image/pho3.jpg)        |![](image/pho4.jpg)


C'était la première fois que j'utilisais le foldscope. Malgré sa simplicité, l'outil demande un certain temps de prise en main. J'ai trouvé la mise en place de la lame compliquée. Il faut jouer avec la rigidité du pliage pour y glisser l'échantillon tout en faisant attention à ne pas trop forcer sur les petites encoches fragiles.

De plus, le foldscope est un outil flexible et la mise au point n'est pas évidente. Il faut d'abord faire une mise au point sur la graduation et puis garder la même position pour faire la mise au point sur la patte, ce qui rend la manipulation délicate.

Mise à part ces quelques difficultés initiales, le foldscope est un outil, que n'importe qui peut maîtriser avec un peu d'entraînement. Il est évident qu'il y a un gros travail de recherche derrière l'user friendliness et j'ai hâte d'approfondir ma maîtrise de cet outil.  


## État de l'art des possibilités techniques et applications du foldscope:

En préparation de la réunion du lundi 10 octobre, nous devions terminer notre recherche bibliographique sur les applications du foldscope. Voici trois articles qui explorent des applications différentes. Le premier article évalue la pertinence du foldscope par rapport à un microscope classique pour une identification de tiques sur le terrain. Le deuxième article utilise le foldscope afin de valider une théorie sur la présence de motif en fougère dans la salive de buffles en chaleur. Le troisième article utilise le foldscope et des algorithme de machine learning pour identifier un pathogène qui provoque une maladie chez les tomates.

En cliquant sur le titre de chacun des articles, vous pourrez accéder à chacune des publications.

**1)** [**Evaluation of foldscope, a paper-based origami microscope useful for taxonomic identification of Rhipicephalus sanguineus ticks**](https://www.actauniversitaria.ugto.mx/index.php/acta/article/view/2134/pdf)

Dans cet article, les auteurs évaluent la pertinence du foldscope pour identifier différentes espèces de tiques à différents stades de développement grâce à des marqueurs visuel morphologiques simples.

L'urbanisation et le réchauffement climatique ont conduit à une expansion des aires d'habitats de nombreuses espèces de tiques. Étant d'importants vecteurs de maladie, on a besoin d'identifier les zones à risques et de contrôler leur évolution pour prévoir des campagnes de prévention efficaces. L'ambition des auteurs est d'accélérer la récolte de donnée pour une mise en place de solutions plus rapides.

Les conclusions de l'article sont prometteuses, le foldscope est un outil pertinent et tout aussi efficace qu'un microscope classique pour supporter cette démarche.

Ils ont étudié le tic *Rhi-picelphalus saguineus*, un tic du chien qui infecte souvent les hommes. Les auteurs en ont collecté 257 sur des chiens à Chihuahua au Mexique pour les analyser avec un microscope classique et puis avec un foldscope légèrement modifié. Le foldscope a permis d'identifier clairement 8 caractéristiques morphologiques que vous pouvez voir sur l'image ci-dessous. 
Grâce à ces marqueurs, chacun des tics s'est vu attribuer un genre, une espèce, un stade d'évolution et un sexe. Les résultat concordent avec ceux obtenus avec un microscope classique.

Marqueurs morphologiques attendus - [crédit image](https://www.inspq.qc.ca/es/node/9553)      |  Observation au foldscope - [crédit image](https://www.actauniversitaria.ugto.mx/index.php/acta/article/view/2134/pdf)
:------------------------------------------:|:--------------------------------------:
![](image/tic3.jpg)                         |![](image/tic2.jpg)



**2)** [**Validation of salivary ferning based estrus identification method in a large population of water buffaloes (Bubalus bubalis) using foldscope**](https://www.sciencedirect.com/science/article/abs/pii/S1642431X21000498)

Le but de cet article est de valider une théorie qui permettrait d'identifier correctement les périodes de chaleur chez les buffles. Pour certaines espèces, on a déjà observé des motifs caractéristiques de cristallisation sur des frottis de salive séchée lors des périodes fertiles chez les femelles. Le but de l'étude est de mettre en évidence le même genre de motifs pour les buffles. L'étude utilise un foldscope comme outil pour repérer ces dessins sur le terrain.

Motif caractéristique recherché en fougère sur les frottis de salive séché - [crédit image](https://www.sciencedirect.com/science/article/abs/pii/S1642431X21000498) |
:---------------------------------------------:|
![](image/foug2.jpg)                               |


Le buffle est le principal producteur de lait dans les pays en voie de développement. Pour contrôler efficacement leur population, l'identification des périodes de chaleur est cruciale, car elle permet d'inséminer les femelles au bon moment. Chez les buffles, la courte durée des chaleurs (5-27h) et l'absence de signes comportementaux clair rend cette tâche particulièrement difficile. Environ 30 pour cent des buffles sont donc inséminé au mauvais moment ce qui entraîne des pertes économiques considérables pour les éleveurs. Dès lors une méthode simple, précise et non-invasive pour détecter les périodes de chaleur est nécessaire.

L'article finit par valider la théorie des auteurs. Pour arriver à ces conclusions, ils ont étudié quatre groupes de buffles d'eau *Bubalus bubalis* dans des conditions différentes. Ils ont récolté 583 échantillons de salive qu'ils ont analysés soit avec un foldscope soit avec un microscope classique en fonction des conditions de terrain. Ils ont ensuite utilisé d'autres techniques pour confirmer ou infirmer les résultats obtenus avec les tests de salive. Au final, ils ont conclu que le motif unique en fougère caractérise bien la période de chaleur avec une précision pouvant aller jusqu'à 91 pour cent pour un des groupes.

Observation au foldscope - femelle en chaleur - [crédit image](hhttps://www.sciencedirect.com/science/article/abs/pii/S1642431X21000498)      |  Observation au foldscope - femelle normale [crédit image](https://www.sciencedirect.com/science/article/abs/pii/S1642431X21000498)
:------------------------------------------:|:--------------------------------------:
![](image/foug3.jpg)                         |![](image/foug4.jpg)

**3)** [**Intelligent disease detection system for early blight of tomato using foldscope**](https://ieeexplore.ieee.org/abstract/document/8986736)

Dans cet article, les auteurs testent une méthode de machine learning pour identifier le pathogène *Alternaria solani* qui provoque une maladie fongique chez les tomates. À l'aide d'un foldscope et d'un smartphone, ils photographient des feuilles de tomates infectées afin de tester quatre algorithmes différents de machine learning qui essaient d'identifier le pathogène.

Le rendement des récoltes est étroitement lié à la santé générale des tomates. Leur production est largement influencée par différents parasites dont l'Alterniaria solani. Avoir un moyen d'identification claire sur le terrain pourrait permettre d'appliquer les pesticides appropriés au bon moment afin d'éviter que les pathogènes ne se rependent sur toute la culture.

Les auteurs arrivent à faire de très bonnes prédictions. Les algorithmes permettent de classifier une image comme "Alternaria solani" ou comme "Non-Alternaria solani" avec 89 pour cent d'efficacité pour un des algorithmes testé. Les auteurs sont satisfaits et pensent que leur travail peut être étendu pour d'autres types d'infections.

Pour faire ces prédictions, les auteurs commencent par entraîner leur algoritmes de détection en se basant sur 100 images dont 60 sont infectées par le pathogène et 40 par d'autres pathogènes. 
Ensuite, les auteurs ont collecté des spécimens en mauvaise santé, (identification par des taches noires sur les feuilles), qu'ils ont  photographiés à l'aide d'un smartphone et d'un foldscope. Les images sont ensuite données aux différents algorithmes pour une classification et les performances sont mesurées. 

Schématisation du procédé - [crédit image](https://ieeexplore.ieee.org/abstract/document/8986736)|
:---------------------------------------------:|
![](image/tom2.jpg)    


Différents résultats obtenus pour les différents algorithmes - [crédit image](https://ieeexplore.ieee.org/abstract/document/8986736)|
:---------------------------------------------:|
![](image/tom6.jpeg)                               |


## Lundi 10 octobre 2022

Nous avons commencé la séance par un tour de table où chacun présentait les différentes applications du foldscope qu'il avait investigué. La discussion a majoritairement tourné autour du médicale et des comparaisons avec la microscopie conventionnelle. L'application que j'ai trouvé la plus originale a été trouvée par [Nasra](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/nasra.daher) qui parle de l'utilisation du foldscope sur le terrain pour détecter du safran de contrefaçon. Cette technique permet une détection rapide et fiable qui aide les acheteurs à ne pas se faire arnaquer.

Nous avons ensuite passé l'après-midi à utiliser le foldscope attaché à un smartphone pour faire plusieurs observations en binôme afin de développer notre maîtrise de l'outil.

Voici quelques observations que [Danaé](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/danae.valdenaire) et moi avons faites :

Épine de Cactus            |Oeil de mouche              |Patte de mouche
:-------------------------:|:-------------------------: |:-------------------------:
![](image/3.2.jpg)         |![](image/4.jpg)            |![](image/2.1.jpg)

Ces images sont remarquables, car elles permettent de mettre en évidence plusieurs détails invisibles à l'œil nu. Pour l'épine, nous pouvons voir les poils en oblique qui aident le cactus à récupérer de l'eau. Pour la mouche on distingue la structure interne hexagonale de son oeil, ses poils et la forme particulière du bout de sa patte.

Nous avons eu besoin d'un certain temps afin de convenablement prendre l'outil en main. Plus nous l'utilison, plus il est facile et intuitif. De plus, il s'assouplit au fur et à mesure et les mouvements qui se faisaient par à-coups deviennent plus fluides.

Comme on peut le remarquer ci-dessus, nous avons eu un problème concernant le cadrage des photos. Pour l'épine, l'objectif du téléphone n'était pas aligné avec l'ouverture du microscope. Ce problème est lié au téléphone que nous avons utilisé. C'est un IPhone qui possède un aimant au centre qui joue avec l'alignement du microscope et du téléphone. Après avoir sécurisé le téléphone avec du papier collant nous avons réussi à mieux cadrer nos images comme on peut le voir sur les photos de la patte et l'œil de mouche.

Lors de notre utilisation, nous avons mis en évidence quelques problèmes techniques liés au foldscope :

- **Problème de stabilité** : La lame de papier bouge sans cesse lorsque nous manipulons le microscope. Afin de résoudre ce problème nous avons mis plusieurs lames enssemble afin d'épaissir le papier. Une autre solution serait d'utiliser des lames en verre.
- **Problème de mise au point**: Le système utilisé pour la mise au point du microscope n'est pas précis. Le pliage qui écarte le microscope de l'échantillon est trop mou pour offrir une mise au point exacte. Ce problème peut se résoudre en remplaçant cette pièce par une quelque chose d'équivalent en plastique dur. 

Malgré ces légers problèmes techniques, le foldscope est un outil qui marche merveilleusement bien.

Nous avons conclu la séance par une discussion sur les aspects techniques du foldscope que nous pourrions adapter et inclure dans le projet parent. Dans l'optique de creuser certains points, nous sommes chacun repartit avec une tâche à effectuer pendant une semaine. J'ai choisi d'aller sur le terrain, de faire un maximum d'observations pour faire un rapport de mon utilisation du foldscope en conditions réelles.

## Partons à l'aventure pour améliorer le foldscope.

### Jour 1

Pour commencer, j'ai décidé de partir dans les bois avec un set up minimaliste. L'objectif, c'est de faire une mesure précise avec le moins de matériel possible. Pour ça, j'ai pris le foldscope, du papier collant et des lames. J'ai fabriqué les lames en amont avec du papier légèrement cartonné et une perforatrice. Le papier collant, que je coupe avec mes dents, sert à fixer l'échantillon dans le trou des lames. J'ai décidé d'observer des feuilles de chêne, de charme, de tilleul et de hêtre. Mon but est de voir les différences à l'échelle du foldscope. Ça a été beaucoup plus difficile que ce que j'avais prévu mais j'ai finalement réussi à reconnaitre les différentes géométries des rainures pour chaque espèce.

Voici les difficultés que j'ai rencontrées et ce que je dois changer pour faciliter mon expérience.

- Matériel manquant : J'aurais dû prévoir une pince pour manipuler l'échantillon que je voulais observer. Le prendre avec les doigts et le placer sur du papier collant est une tâche difficile qui laisse des traces indésirables. J'aurais aussi dû prévoir un petit sac, transporter le matériel pendant 2 h dans ses mains, c'est pas pratique et ca devient vite gênant. Par exemple lorsque je manipulais un échantillon, j'étais obligé de poser toutes mes affaires sur le sol. Enfin, j'aurais dû prendre la LED avec moi. J'avais oublié qu'il faisait plus sombre dans les bois et j'ai à chaque fois dû trouver un coin plus dégagé pour faire mes observations.

- Logistique des observations : Pour cette première session j'ai travailler échantillon par échantillon, c'est à dire que je trouvais un échantillon, je le préparais et puis je l'observais. Je crois qu'il sera plus efficace de travailler par bloc. C'est-à-dire de d'abord trouver mes échantillons, de les préparer ensemble et enfin de les observer tous d'un coup.

- Ressentit général : Le plus difficile lors de cette première session, c'était la préparation de l'échantillon. Cette tâche pourtant anodine en laboratoire, nécessite une grande maitrise et de l'agilité lorsqu'on est debout dans les bois avec du vent. J'ai plusieurs fois fait tomber mes échantillons ou raté une observation parce qu'ils étaient mal placés ou sales. J'espère que ma nouvelle logistique d'observations m'aidera à être plus performant. De plus, le mouvement de l'échantillon dans le foldscope était saccadé à cause d'un accrochage systématique entre une des ouvertures aimanté et la lame. Pour finir, le papier cartonné que j'ai utilisé était trop fin, ma lame bougeait sans arrêt et les observations devenaient vite frustrantes.

### Jour 2

Pour cette deuxième journée, j'ai décidé de refaire la même chose que la veille, mais en complétant mon matériel par ce qu'il me manquait et en changeant ma logistique comme expliqué ci-dessus. J'ai aussi essayé deux améliorations en changeant le papier cartonné par du papier plus épais et en lissant avec du papier collant les bords d'une des ouverture aimanté pour un mouvement plus souple.

Avant modification - Mouvement saccadé      |  Après modification - Mouvement souple
:------------------------------------------:|:--------------------------------------:
![](image/b.jpg)                            |![](image/a.jpg)

Je suis retourné au même endroit que la veille et j'ai pris mes quatre échantillons d'un coup. La pince s'est avérée très utile et m'a permis d'être beaucoup plus précis lors des manipulations. J'ai alors préparer mes quatre lames et en 10 minutes, j'étais déjà prêt à faire mes observations, que j'ai faites avec plus d'aisance que la veille.

Cette fois-ci, j'avais tout. J'étais équipé d'un sac banane dans lequel se trouvais mon foldscope, du papier collant, la lampe LED et mes échantillons récoltés au fur et à mesure. Ma nouvelle logistique marchait à merveille. Le fait de préparer mes échantillons d'un coup m'a fait gagner beaucoup de temps en automatisant les gestes techniques. Enfin, les mouvements étaient beaucoup plus souples grâce au lissage des bords de l'ouverture aimanté. Cependant, le problème de la lame qui bouge était toujours là, malgré le papier plus épais.

Afin de résoudre ce problème, je vais essayer d'utiliser de la pâte adhésive afin d'encrer la lame dans ses encoches.

foldscope avec pâte adhésive sous les encoches |
:---------------------------------------------:|
![](image/d.jpg)                               |


### Jour 3

Pour varier les plaisirs, j'ai décidé de changer d'observation et d'aller dans les champs plutôt que dans les bois. J'étais rentré tard de mes cours et je savais que la nuit allait tomber pendant mes observations. C'était l'occasion rêvée pour manipuler le foldscope avec peu de lumière.
Une fois dans les champs, j'ai décidé d'observer différents insectes que je trouvais par terre. J'ai alors commencé une chasse aux insectes qui s'est avérée peu fructueuse et je me suis rabattu sur l'observation d'une tête de fourmi, d'un champignon, d'un peu de terre et d'une punaise. La préparation des échantillons a été rapide, mais il faisait déjà sombre quand j'ai commencé ma première observation.

À première vue, la fixation de la lame avec la pâte adhésive semblait être la solution miracle. La lame rentre facilement et reste bien en place. J'ai pu faire une magnifique observation de la tête de fourmi en distinguant bien les antennes et les mandibules, tout en bougeant d'un côté à l'autre de l'échantillon avec un mouvement souple et précis.

Les problèmes arrivent lorsque je veux retirer la lame pour commencer ma seconde observation. J'aurais dû m'en douter, mais la lame était trop bien attachée. Après avoir réussi à la retirer, la pâte adhésive n'était plus sous les encoches, mais sur la lame. Il a suffi de quatre observations en suivant ce procédé, pour que ma solution miracle ne colle quasi plus. Au final cette fausse bonne idée demandait beaucoup plus de travail et j'ai donc décidé de l'abandonner.

Pour finir sur une bonne note, l'observation de nuit ne m'a en revanche posé aucun problème. Mes échantillons étaient déjà prêts avant qu'elle ne tombe complétement et mes observations étaient belles car mes yeux étaient habitué à l'obscurité. La lumière précise de la LED faisait ressortir de magnifiques détails bien tranchants.

### Jour 4

Le plus gros frein pour une utilisation fluide du foldscope, c'est le changement de lame. Le foldscope n'est pas adapté et il faut à chaque fois se battre avec la rigidité des encoches pour fixer le nouvel échantillon. Ce n'est pas flagrant lorsqu'on fait quelques observations au chaud dans un laboratoire. En revanche, sur le terrain, ce processus devient vite laborieux et chronophage.  

Pour ce quatrième jour, j'ai donc décidé de m'attaquer au problème du changement de lames et de leur fixation. J'ai emprunté un chemin radicalement différent. L'idée, c'est de mettre une lame en verre qui reste dans le foldscope tout au long de la journée. Cette lame tient beaucoup mieux en place que les lames en carton. À chaque fois que je veux observer un échantillon, je le mets en sandwich entre deux couches de papier collant, que je retire une fois l'observation effectuée. 

L'avantage d'utiliser deux couches, c'est que verre ne se salit pas et lorsque j'ai fini, je dois simplement retirer le sandwich de papier collant pour que mon foldscope soit prêt à recevoir un nouvel échantillon. Notons que cette méthode impose que j'abandonne ma routine initiale qui consistait à tout préparer d'un coup.

J'ai alors été me promener dans un parc et j'ai observé les bouts de plastique que je trouvais sur mon chemin. J'ai trouvé des morceaux de plastique bleu, rouge et transparent. Ces observations n'étaient pas très intéressantes mais elles m'ont permis de tester ma nouvelle idée qui fonctionne à merveille. Malgré un changement de routine, ma dernière trouvaille fluidifie et facilite l'utilisation du foldscope. En plus du gain attendu, j'ai trouvé mon foldscope plus précis, car le verre rigidifie l'outil. C'est une réussite !

Petit bémol, le verre, c'est fragile et voici ce qui arrive à un foldscope trimballé au fond d'une sacoche :

![](image/e.jpg)


### Jour 5

Pour ce dernier jour d'observation, je n'avais pas beaucoup de temps et je me suis dit que j'allais refaire le parcours du premier jour pour voir comment mon outil et ma maîtrise de celui-ci avaient évolué. Afin de ne plus casser d'autres lames, j'ai attaché une ficelle au foldscope pour qu'il me pende au cou comme un collier.

Lors de ce cinquième jour, il pleuvait. J'ai eu du mal à coller mes échantillons sur le verre humide. Malgré ça, les observations se sont bien déroulées. J'ai été beaucoup plus rapide que la veille. L'ajout du collier s'avère être une solution géniale, le foldscope est maniable et est toujours à portée de main.

## Le mot de la fin

Cette journée pluvieuse conclut ma petite semaine d'observation sur le terrain. Je suis content de ma maîtrise du foldscope et des modifications que j'ai apporté à celui-ci. C'était déjà un outil génial et ces dernières modifications ne font que mettre en évidence le génie de Manu Prakash. Le fait d'utiliser une lame en verre a radicalement changé mon utilisation du foldscope. Il devient super intuitif et on ne doit plus manipuler ses entrailles pour faire une d'observation. L'ajout du collier combiné a cette lame de verre me permet de lancer de nouvelles observations en 5 minutes top chrono. 

Comme mentionné précédemment, j'ai créé un petit tutoriel qui reprend les enseignements que j'ai tirés de mon expérience. Il permet à n'importe qui d'être autonome, efficace et rapide sur le terrain. Il a été rédigé en anglais pour toucher un public plus large et se trouve joint au projet sous le format pdf.  

Grâce à ce travail, j'ai découvert les sciences frugales. Face à un monde encore très inégalitaire, où les bouleversements climatiques signent la fin d'une certaine abondance, elles apparaissent comme une clef indispensable pour nous aider à vaincre les défis de demain.

Au final, mon exploration personnelle n'aide pas concrètement le projet parent, mais elle constitue, je l'espère, une brique sur laquelle mes lecteurs peuvent s'appuyer pour partir d'un peu plus haut.

Simon Biot 
